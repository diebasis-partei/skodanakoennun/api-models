package de.diebasis.skodanakoennun.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Poll {

  private int id;
  private String title;
  private String description;
  private List<Proposal> proposals;
  private long created;
  private long expires;
  private PollStatus status;

}
