package de.diebasis.skodanakoennun.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChoiceSchema {

  private int id;
  private int min;
  private int max;
  private boolean multiValue;
  private boolean noOpinion;

}
