package de.diebasis.skodanakoennun.models;

public enum PollStatus {
  DRAFT("draft"),
  ACTIVE("active"),
  EXPIRED("expired"),
  CLOSED("closed"),
  ARCHIVE("archive");

  public final String value;

  PollStatus(String value) {
    this.value = value;
  }
}
