package de.diebasis.skodanakoennun.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Proposal {

  private int id;
  private int pollId;
  private String title;
  private String description;
  private ChoiceSchema choiceSchema;

}
